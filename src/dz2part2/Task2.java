package dz2part2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.
Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями (см. пример) и вывести всю матрицу на экран.
Ограничения:
● 0 < N < 100
● 0 <= X1
, Y1
, X2
, Y2 < N
● X1 < X2
● Y1 < Y2

 */

import java.util.Scanner;

public class Task2 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] a1 = new int[2][2];
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                a1[i][j] = scanner.nextInt();
            }
        }
        int[][] a2 = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a2[i][j] = 0;
                if(i >= a1[0][1] && i <= a1[1][1]){
                    if(i == a1[0][1] || i == a1[1][1]){
                        if(j >= a1[0][0] && j <= a1[1][0]){
                            a2[i][j] = 1;
                        }
                    } else if (j == a1[0][0] || j == a1[1][0]){
                        a2[i][j] = 1;
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if(j < n - 1){
                    System.out.print(a2[i][j] + " ");
                } else {
                    System.out.print(a2[i][j]);
                }
            }
            System.out.println();
        }
    }
}
