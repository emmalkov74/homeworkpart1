package pmdz1.Task4;
/*
Создать класс MyEvenNumber, который хранит четное число int n. Используя
исключения, запретить создание инстанса MyPrimeNumber с нечетным числом.
 */

import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите целое четное число: ");
        try {
            MyEvenNumber myEvenNumber = new MyEvenNumber(scanner.nextInt());
            System.out.println(myEvenNumber);
        }catch (ArithmeticException e){
            System.out.println(e.getMessage());
        }


    }
}
