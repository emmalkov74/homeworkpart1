package pmdz1.Task4;

/**
 * Класс для хранения Вашего целого четного числа, в случае попытки создания в конструкторе класса объекта с нечетным
 * числом пробрасывается исключение ArithmeticException с соответствующим сообщением
 */
public class MyEvenNumber {
    // целое четное число
    private int n;

    /**
     * Конструктор принимающий целое четное число
     * @param n целое четное число
     */
    public MyEvenNumber (int n) {
        if (n % 2 != 0){
            throw new ArithmeticException("Создание инстанса MyPrimeNumber с нечетным числом запрещено");
        }
        this.n = n;
    }


    /**
     * Переопределенный метод возвращающий объект класса в виде строки
     * @return объект класса в виде строки
     */
    @Override
    public String toString () {
        return "MyEvenNumber{" +
                "n=" + n +
                '}';
    }
}
