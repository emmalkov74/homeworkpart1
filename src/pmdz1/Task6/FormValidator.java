package pmdz1.Task6;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import static java.util.regex.Pattern.matches;

/**
 *
 */
public class FormValidator {

    /**
     *Метод валидирующий длину имени, она должна быть от 2 до 20 символов, первая буква заглавная, если эти условия
     * нами не выполняется выбрасываем FormValidatorException
     * @param str имя
     * @throws FormValidatorException исключение которое мы выбрасываем в случае неправильного заполнения данного поля формы
     */
    public static void checkName(String str) throws FormValidatorException {
        if(!matches("[А-я][а-я]{1,18}", str)){
            throw new FormValidatorException("Длина имени должна быть от 2 до 20 буквенных символов, первая буква заглавная");
        }
    }

    /**
     * Метод валидирующий дату рождения, она должна быть в формате dd.mm.yyyy и быть не раньше 01.01.1900 и не позже текущей даты
     * @param str дата
     * @throws FormValidatorException исключение которое мы выбрасываем в случае неправильного заполнения данного поля формы
     */
    public static void checkBirthdate(String str) throws FormValidatorException {
        if(!matches("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d", str)){
            throw new FormValidatorException("Дата рождения должна быть в формате dd.mm.yyyy");
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate date = LocalDate.parse(str, dateTimeFormatter);
        LocalDate now = LocalDate.now();
        LocalDate minDate = LocalDate.parse("01.01.1900", dateTimeFormatter);
        if (date.isBefore(minDate) || date.isAfter(now)){
            throw new FormValidatorException("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты");
        }
    }

    /**
     * Метод валидирующий пол, он должен быть мужской или женский
     * @param str пол
     */
    public static void checkGender(String str) throws FormValidatorException {
        if (!(str.equals(Gender.MALE.getName()) || str.equals(Gender.FEMALE.getName()))){
            throw new FormValidatorException("Пол должен быть мужской или женский");
        }
    }

    /**
     * Метод валидирующий рост, он должен быть положительным числом и корректно конвертироваться в double
     * @param str рост
     */
    public static void checkHeight(String str) throws FormValidatorException {
        Double height = Double.parseDouble(str);
        if(height < 0){
            throw new FormValidatorException("Рост должен быть положительным числом");
        }
    }


}
