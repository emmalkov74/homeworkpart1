package pmdz1.Task6;

/**
 * Перечисление созданное для однозначного определения пола мужской или женский
 */
public enum Gender {
    MALE("мужской"),
    FEMALE("женский");

    private String name;

    Gender (String name) {
        this.name = name;
    }

    public String getName () {
        return name;
    }
}
