package pmdz1.AddTask2;
/*
На вход подается число n, массив целых чисел отсортированных по
возрастанию длины n и число p. Необходимо найти индекс элемента массива
равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
вывести -1.
Решить задачу за логарифмическую сложность.

5
-42 -12 3 5 8
3
 */

import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner  = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int n = scanner.nextInt();
        int[] arr = new int[n];
        System.out.println("Введите " + n + " значений массива отсортированных по\n" +
                "возрастанию: ");
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println("Введите число которое будем искать в массив: ");
        int p = scanner.nextInt();

        int desiredIndex = findIndex(arr, p);

        System.out.println(desiredIndex);


    }

    /**
     * Метод реализующий алгоритм двоичного поиска имеющий сложность O(log n)
     * @param arr массив в котором ищем искомое число
     * @param p искомое число
     * @return  индекс искомого числа или -1 если такового в массиве нет
     */
    private static int findIndex (int[] arr, int p) {
        int index = -1;
        int low = 0;
        int high = arr.length - 1;
        while (low <= high){
            int mid = low + ((high - low)) / 2;
            if(arr[mid] < p){
                low = mid + 1 ;
            } else if(arr[mid] > p){
                high = mid - 1;
            } else if (arr[mid] == p){
                index = mid;
                break;
            }
        }
        return index;
    }

}
