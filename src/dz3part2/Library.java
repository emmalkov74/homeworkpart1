package dz3part2;

import java.util.ArrayList;
import java.util.Scanner;

public class Library {

    private final ArrayList<Book> listLibraryBook;// список книг в библиотеке

    private int lastId; //содержит Id последнего посетителя, если они были и одалживали книги

    /**
     * Конструктор создающий библиотеку с пустым списком книг
     */
    public Library () {
        this.listLibraryBook = new ArrayList<>();
        this.lastId = 0;
    }


    /**
     * Возвращает список книг в библиотеке
     * @return список книг в библиотеке
     */
    public ArrayList<Book> getListLibraryBook () {
        return listLibraryBook;
    }

    /**
     * Добавляет книгу в библиотеку, если книги с таким названием в ней еще нет
     * @param book книга
     */
    public void addBookToLibrary(Book book){
        if (listLibraryBook.isEmpty() || !isBookInLibrary(book.getName())){
            listLibraryBook.add(book);
        } else {
            System.out.println("Книга с названием " + book.getName() + " уже существует в библиотеке");
        }
    }

    /**
     * Проверяет есть ли книга с таким названием в библиотеке
     * @param name название
     * @return true или false, если книги еще нет
     */
    private boolean isBookInLibrary(String name){
        for (Book book: listLibraryBook) {
            if (book.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    /**
     * Выводит в консоль список книг переданный в качестве параметра
     * @param newList список книг
     */
    public static void printListBooks(ArrayList<Book> newList){
        System.out.println("ID \t Автор \t\t\t Наименование");
        System.out.println("----------------------------------");
        for (Book book: newList) {
            System.out.printf("%d \t %s \t %s \n", book.getReaderId(),book.getAuthor(), book.getName() );
        }
        System.out.println();
    }

    /**
     * Возвращает книгу по введенному названию, если книг с таким названием в библиотеке нет возвращает null
     *  и выводит соответствующее сообщение
     * @param name название
     * @return книга
     */
    public Book findBookByName(String name){
        for (Book book: listLibraryBook) {
            if (book.getName().equals(name)){
                return book;
            }
        }
        System.out.println("Книги с названием " + name + "пока нет в библиотеке");
        return null;
    }

    /**
     * Возвращает список книг по введенному автору, если книг с таким автором в библиотеке нет возвращает null
     *  и выводит соответствующее сообщение
     * @param author автор
     * @return список книг
     */
    public ArrayList<Book> findBooksByAuthor(String author){
        ArrayList<Book> newList = new ArrayList<>();
        for (Book book: listLibraryBook) {
            if (book.getAuthor().equals(author)){
                newList.add(book);
            }
        }
        if (!newList.isEmpty()){
            return newList;
        }
        System.out.println("Книг с автором " + author + "пока нет в библиотеке");
        return null;
    }


    /**
     * Удаляет книгу из библиотеки по названию, если она в библиотеке есть и не одолжена
     * @param name название
     */
    public void removeBookFromLibrary (String name) {

        if ( listLibraryBook.isEmpty() || isBookInLibrary(name)){
            Book toRemoveBook = findBookByName(name);
            if (toRemoveBook.getReaderId() == 0){
                listLibraryBook.remove(toRemoveBook);
            } else {
                System.out.println("Книгу с названием "+ name +"нельзя удалить из библиотеки пока она одолжена");
            }
        } else {
            System.out.println("Книгу  с названием " + name + " нельзя удалить из библиотеки, т.к. библиотека" +
                    " либо пуста, либо в ней нет такой книги");
        }
    }

    /**
     * Метод одалживания книги посетителю. Каждый посетитель в один момент времени может читать только одну книгу.
     * Одолжить книгу посетителю по названию можно, если выполнены все условия:
     * a. Она есть в библиотеке.
     * b. У посетителя сейчас нет книги.
     * c. Она не одолжена.
     * Также если посетитель в первый раз обращается за книгой — дополнительно выдать ему идентификатор читателя.
     * @param libraryReader посетитель
     * @param name название книги
     */
    public void checkOutBook(LibraryReader libraryReader, String name){
        if(isBookInLibrary(name)){
            if(libraryReader.getBookName().equals("")){
                Book book = findBookByName(name);
                if(book.getReaderId() == 0){
                    if(libraryReader.getId() == 0){
                        libraryReader.setId(++lastId);
                    }
                    libraryReader.setBookName(book.getName());
                    book.setReaderId(libraryReader.getId());
                } else {
                    System.out.println("Данная книга уже выдана другому читателю, придется подождать ее возвращения и библиотеку");
                }
            } else {
                System.out.println("Посетитель " + libraryReader.getName() + " уже взял другую Книгу, сначала нужно ее вернуть" );
            }
        } else {
            System.out.println("Книги с таким названием нет в библиотеке");
        }
    }

    /**
     * Метод позволяет вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу и не принимает
     * книгу от другого посетителя.
     * a. Книга перестает считаться одолженной.
     * b. У посетителя не остается книги.
     * @param libraryReader посетитель
     * @param name название книги
     */
    public void bookBack(LibraryReader libraryReader, String name){
        if(isBookInLibrary(name)){
            Book book = findBookByName(name);
            if(libraryReader.getId() == book.getReaderId()){

                Scanner scanner = new Scanner(System.in);
                int i = 0;
                do{
                    System.out.println("Просим ввести оценку возвращаемой книги от 0 до 5: ");
                     i = scanner.nextInt();
                }while (i < 1 || i > 5 );
                book.addNewScoresBook(i);
                libraryReader.setBookName("");
                book.setReaderId(0);
                System.out.println("Книга успешно возвращена.");
            } else {
                System.out.println("Посетитель " + libraryReader.getName() + " не брал эту Книгу, нельзя принять книгу от другого " +
                        "посетителя или Посетитель принес не ту книгу которую он брал"  );
            }
        } else {
            System.out.println("Книги с таким названием нет в библиотеке");
        }
    }
}
