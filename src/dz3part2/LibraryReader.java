package dz3part2;

public class LibraryReader {
    private String name;// имя посетителя
    private int id;//идентификатор, присваивается только после того как он возьмет книгу в библиотеке
    private String bookName;//название книги которую он взял или пустая строка если он пока не вял ни одну книгу

    /**
     * Приватный конструктор чтобы не создавали посетителя без имени
     */
    private LibraryReader(){

    }

    /**
     * Публичный конструктор, создает посетителя с указанным именем
     * @param name имя посетителя
     */
    public LibraryReader (String name) {
        this.name = name;
        this.bookName = "";
    }

    /**
     * Возвращает имя посетителя
     * @return имя посетителя
     */
    public String getName () {
        return name;
    }

    /**
     * Возвращает идентификатор посетителя
     * @return идентификатор посетителя
     */
    public int getId () {
        return id;
    }

    /**
     * Устанавливает идентификатор посетителя
     * @param id  идентификатор посетителя
     */
    public void setId (int id) {
        this.id = id;
    }

    /**
     * Возвращает название книги, которую взял посетитель или пустую строку если он ничего не должен
     * @return
     */
    public String getBookName () {
        return bookName;
    }

    /**
     * Вписывает в соответствующее поле название книги которую взял посетитель
     * @param bookName название книги
     */
    public void setBookName (String bookName) {
        this.bookName = bookName;
    }

    /**
     * Переопределенный метод для выведения на печать объекта
     *  @return строка с полями объекта
     */
    public String toString(){
        return "ID: " + getId() + " Имя: " + getName() + " Книга: " + getBookName() + "\n";
    }
}
