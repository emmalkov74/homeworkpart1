package dz2part1;

import java.util.Scanner;

/*
 Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.
Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
● заглавные латинские символы
● строчные латинские символы
● числа
● специальные знаки(_*-)
 */
public class AddTask1 {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Приглашаем ввести желаемую длину пароля
        System.out.println("Введите длину желаемого пароля: ");
        int n = scanner.nextInt();
        //если введенная длина меньше восьми сиволов просим ввести пароль еще раз
        while (n < 8) {
            System.out.println("Пароль с " + n + " количеством символов небезопасен");
            System.out.println("Введите длину желаемого пароля: ");
            n = scanner.nextInt();
        }
        //Вызываем метод генеации пароля и выводим полученный из него пароль на экран
        System.out.println(generatePassword(n));
    }

    /**
     * @param n длина желаемого пароля
     * @return сгенерированый пароль, удовлетворяющий условиям в виде строки
     */
    private static String generatePassword (int n) {
        String password = ""; //создаем пустую строку наш будующий пароль

        while (password.length() < n){

            //добавляем в пароль заглавную букву
            password += generateLetterOrNumber(65,90);
            if (password.length() == n){
                break;
            }
            //добавляем в пароль строчную букву
            password += generateLetterOrNumber(97, 122);
            if(password.length() == n){
                break;
            }
            //добавляем в пароль цифру
            password += generateLetterOrNumber(48, 57);
            if(password.length() == n){
                break;
            }
            //добавляем в пароль символ
            password += generateSimbol();

        }
        return password;
    }

    /**
     * @return возвращает специальный знак (_*-)
     */
    private static String generateSimbol () {
        String s = "";
        double f = Math.random()/Math.nextDown(1.0);
        int x = (int) ((1.0 - f) + 3 * f);
        switch (x) {
            case 1 -> s += (char) 95;
            case 2 -> s += (char) 42;
            case 3 -> s += (char) 45;
        }
        return s;
    }

    /**
     * @return возвращает случайную заглавную или строчную латинскую букву или цифру в зависмости от входных данных
     */
    private static String generateLetterOrNumber (int from, int to) {
        String s = "";
        double f = Math.random()/Math.nextDown(1.0);
        int x = (int) (from * (1.0 - f) + to * f);
        return s + (char) x;
    }
}
