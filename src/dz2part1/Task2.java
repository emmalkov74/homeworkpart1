package dz2part1;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого аналогично передается второй
массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть
содержат одинаковое количество элементов и для каждого i == j элемент ai ==
aj). Иначе вывести false.
Ограничения:
● 0 < N < 100
● 0 < ai < 1000
● 0 < M < 100
● 0 < aj < 1000
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr1 = new int[n];
        for (int i = 0; i < n; i++) {
            arr1[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] arr2 = new int[m];
        for (int i = 0; i < m; i++) {
            arr2[i] = scanner.nextInt();
        }
        System.out.println((n == m && compareArrays(arr1,arr2)));
    }
    public static boolean compareArrays(int[] arr1, int[] arr2){
        boolean flag = true;
        for (int i = 0; i < arr1.length; i++) {
            if(arr1[i] != arr2[i]){
                flag = false;
                break;
            }
        }
        return flag;
    }
}
