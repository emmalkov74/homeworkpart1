package dz2part1;
/*
 На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
вводится число X — элемент, который нужно добавить в массив, чтобы
сортировка в массиве сохранилась.
Необходимо вывести на экран индекс элемента массива, куда нужно добавить
X. Если в массиве уже есть число равное X, то X нужно поставить после уже
существующего.
Ограничения:
● 0 < N < 100
● -1000 < ai < 1000
● -1000 < X < 100
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        int[] resultArray = new int[n + 1];
        int index = -1;
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int x = scanner.nextInt();
        for (int i = 0; i < n; i++) {
             if(arr[i] > x){
                index = i;
                break;
            }
        }
        System.out.println(index);
    }
}
