package dz1part2;
/*
(1 балл) Дома дочери Пети опять нужна помощь с математикой! В этот раз ей
нужно проверить, имеет ли предложенное квадратное уравнение решение или
нет.
На вход подаются три числа — коэффициенты квадратного уравнения a, b, c.
Нужно вывести "Решение есть", если оно есть и "Решения нет", если нет.
Ограничения:
-100 < a, b, c < 100

 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        if ((b * b - 4 * a * c) >= 0){
            System.out.println("Решение есть");
        } else {
            System.out.println("Решения нет");
        }
    }
}
