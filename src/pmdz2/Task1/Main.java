package pmdz2.Task1;
/*
Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main (String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("Moskov");
        list.add("Ufa");
        list.add("Moskov");
        list.add("Kiev");
        list.add("Moskov");
        list.add("Omsk");
        printList(list);
        System.out.println();
        printList(findUniqueElements(list));

    }

    /**
     * Метод , который на вход принимает ArrayList<T>, а возвращает набор уникальных элементов этого массива.
     * @param list  принимаемый ArrayList
     * @param <T> обобщение, которое будет задавать тип элементов ArrayList
     * @return набор уникальных элементов принимаемого ArrayList
     */
    public static <T> ArrayList<T> findUniqueElements(ArrayList<T> list){
        Set<T> set = new HashSet<>();
        set.addAll(list);
        ArrayList<T> uniqueElementList = new ArrayList<>();
        uniqueElementList.addAll(set);
        return uniqueElementList;
    }

    public static <T> void printList(ArrayList<T> list){
        list.forEach(e -> System.out.print(e + " "));
    }



}


