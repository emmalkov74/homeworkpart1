package pmdz2.Task2;
/*
С консоли на вход подается две строки s и t. Необходимо вывести true, если одна строка является валидной анаграммой
другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв другого слова или фразы, обычно с использованием
всех исходных букв ровно один раз
 */

import java.util.*;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первую строку: ");
        String s = scanner.next();
        System.out.println("Введите вторую строку: ");
        String t = scanner.next();

        System.out.println(isAnagram(s, t));
    }

    /**
     *Метод, который принимает на вход две строки и выводит true, если одна строка является валидной анаграммой другой
     * строки и false иначе.
     * @param s первая строка
     * @param t вторая строка
     * @return true, если одна строка является валидной анаграммой второй строки и false иначе.
     */
    private static boolean isAnagram (String s, String t) {

        if(s.length() != t.length()){
            return false;
        }
        char[] arrS = s.toCharArray();
        char[] arrT = t.toCharArray();
        List<Character> listS = new ArrayList<>();
        for (char c: arrT){
            listS.add(c);
        }
        ArrayList<Character> listT = new ArrayList<>();
        for (char c: arrT){
            listT.add(c);
        }
        for (Character c: arrS) {
            int index;
            if ((index = listT.indexOf(c)) == -1){
                return false;
            }else {
                listT.remove(index);
            }
        }
        return true;
    }
}
