package dz3part1.Task8;

public class Main {
    public static void main (String[] args) {
        Atm atm1 = new Atm(Currency.RUB, 65.0,Currency.USD);
        System.out.printf("%.2f \n", atm1.exchangeDollarsToRubles(365.0));
        System.out.printf("%.2f \n", atm1.exchangeRublesToDollars(32.0));

        Atm atm2 = new Atm(Currency.USD, 0.5, Currency.RUB);
        System.out.printf("%.2f \n", atm2.exchangeDollarsToRubles(365.0));
        System.out.printf("%.2f \n", atm2.exchangeRublesToDollars(32.0));

        System.out.println(Atm.getCounterInstances());
    }
}
