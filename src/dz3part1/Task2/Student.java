package dz3part1.Task2;
/*
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но
не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name
● геттер/сеттер для surname
● геттер/сеттер для grades
● метод, добавляющий новую оценку в grades. Самая первая оценка
должна быть удалена, новая должна сохраниться в конце массива (т.е.
массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)

 */

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

public class Student {

    private String name; //имя студента
    private String surname; //фамилия студента
    private int[] grades; //последние 10 оценок студента. Их может быть меньше, но не может быть больше 10.


    /**
     * Конструктор по умолчанию, создает студента с пустыми полями
     */
    public Student () {
        this.name = "";
        this.surname = "";
        this.grades = new int[]{};
    }


    /**
     * Конструктор, который создает студента с заданными полями
     * @param name имя студента
     * @param surname фамилия студента
     * @param grades последние 10 оценок студента.
     */
    public Student (String name, String surname, int[] grades) {
        this.name = name;
        this.surname = surname;
        this.grades = grades;
    }

    /**
     * @return имя студента
     */
    public String getName () {
        return name;
    }

    /**
     * @param name имя студента
     */
    public void setName (String name) {
        this.name = name;
    }

    /**
     * @return фамилия студента
     */
    public String getSurname () {
        return surname;
    }

    /**
     * @param surname фамилия студента
     */
    public void setSurname (String surname) {
        this.surname = surname;
    }

    /**
     * @return последние 10 оценок студента или сколько есть
     */
    public int[] getGrades () {
        return grades;
    }

    /**
     * @param grades последние 10 оценок студента или сколько есть
     */
    public void setGrades (int[] grades) {
        if (grades.length > 10){
            this.grades = Arrays.copyOfRange(grades,grades.length - 11, grades.length - 1 );
        } else {
            this.grades = grades;
        }

    }

    /**
     * метод, добавляющий новую оценку в grades. Самая первая оценка
     * должна быть удалена, новая должна сохраниться в конце массива (т.е.
     * массив должен сдвинуться на 1 влево).
     * @param grade оценка
     */
    public void addNewGrade(int grade){
        if(grades.length < 10){
            int[] newGrades = Arrays.copyOf(grades, grades.length + 1);
             newGrades[newGrades.length - 1] = grade;
             grades = newGrades;
        } else {
            for (int i = 1; i < grades.length; i++) {
                grades[i - 1] = grades[i];
            }
            grades[grades.length - 1] = grade;
        }
    }

    /**
     * @return средний балл студента (рассчитывается как
     * среднее арифметическое от всех оценок в массиве grades)
     */
    public double arithmeticMean (){
        int sumGrades = 0; // сумма оценок
        for (int grade : grades) {
            sumGrades += grade;
        }
        return (double) sumGrades / grades.length;
    }
}
