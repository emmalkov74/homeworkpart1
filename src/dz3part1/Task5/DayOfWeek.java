package dz3part1.Task5;
/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…
7 Sunday
 */

public class DayOfWeek {
    private byte numberOfDay;//порядковый номер дня недели
    private String dayOfWeek;//название дня недели

    /**
     * Конструктор создает объект заданными номером и названием дня недели
     * @param numberOfDay номер дня недели
     * @param dayOfWeek название дня недели
     */
    public DayOfWeek (byte numberOfDay, String dayOfWeek) {
        this.numberOfDay = numberOfDay;
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * Возвращает номер дня недели
     * @return номер дня недели
     */
    public byte getNumberOfDay () {
        return numberOfDay;
    }


    /**
     * Возвращает название дня недели
     * @return название дня недели
     */
    public String getDayOfWeek () {
        return dayOfWeek;
    }


}
