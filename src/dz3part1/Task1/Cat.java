package dz3part1.Task1;

public class Cat {


    /**
     * выводит на экран “Sleep”
     */
    private static void sleep(){
        System.out.println("Sleep");
    }
    /**
     * выводит на экран “выводит на экран “Meow””
     */
    private static void meow(){
        System.out.println("Meow");
    }
    /**
     * выводит на экран “выводит на экран “Eat”
     */
    private static void eat(){
        System.out.println("Eat");
    }
    /**
     * вызывает один из приватных методов случайным образом
     */
    public static void status(){
        int i = (int) (Math.random() * 3);

        switch (i){
            case 0:
                meow();
                break;
            case 1:
                sleep();
                break;
            case 2:
                eat();
                break;
        }
    }



}
