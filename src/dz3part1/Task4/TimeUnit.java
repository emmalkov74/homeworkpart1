package dz3part1.Task4;
/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
(необходимые поля продумать самостоятельно). Обязательно должны быть
реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате
(используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в
TimeUnit (на вход передаются только часы, минуты и секунды).

 */

public class TimeUnit {
    private int hours; //  часы
    private int minutes;// минуты
    private int seconds;// секунды

    /**
     * создает объект TimeUnit, задав часы, минуты и секунды
     * @param hours часы
     * @param minutes минуты
     * @param seconds секунды
     */
    public TimeUnit (int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    /**
     * создает объект TimeUnit, задав часы и минуты. Секунды
     * должны проставиться нулевыми.
     * @param hours  часы
     * @param minutes минуты
     */
    public TimeUnit (int hours, int minutes) {
        this.hours = validateHors(hours);
        this.minutes = validateMinutesOrSeconds(minutes);
        this.seconds = 0;
    }



    /**
     * создает объект TimeUnit, задав часы. Минуты и секунды
     * должны проставиться нулевыми.
     * @param hours часы
     */
    public TimeUnit (int hours) {
        this.hours = validateHors(hours);
        this.minutes = 0;
        this.seconds = 0;
    }

    /**
     * Проверяет диапазон введенного значения часов. Если введенное значение больше 24 или меньше нуля возвращает ноль
     * и пишет соответствующее сообщение.
     * @param hours проверяемое значение часов
     * @return возврааемое значение часов
     */
    private int validateHors (int hours) {
        if (hours > 0 && hours < 24){
            return hours;
        } else {
            System.out.println("Введенное значение " + hours + ",а должно быть больше нуля и меньше 24," +
                    " поэтому будет установлено как ноль");
            return 0;
        }
    }

    /**
     * Проверяет диапазон введенного значения минут или секунд. Если введенное значение больше 60 или меньше нуля возвращает ноль
     * и пишет соответствующее сообщение.
     * @param value проверяемое значение минут или секунд
     * @return возврааемое значение минут или секунд
     */
    private int validateMinutesOrSeconds (int value) {
        if(value > 0 && value < 60){
            return value;
        } else {
            System.out.println("Введенное значение " + value + ",а должно быть больше нуля и меньше 60," +
                    " поэтому будет установлено как ноль");
            return 0;
        }

    }

    /**
     * Выводит на экран установленное в классе время в формате hh:mm:ss
     */
    public void printTimeIn24HoursFormat() {
        System.out.println(helpPrintMethod(hours)  + ":" + helpPrintMethod(minutes)  + ":" + helpPrintMethod(seconds));
    }

    /**
     * Выводит на экран установленное в классе время в 12-часовом формате
     * (используя hh:mm:ss am/pm)
     */
    public void  printTimeIn12HoursFormat() {
        System.out.println((hours > 12 ? helpPrintMethod(hours - 12) : helpPrintMethod(hours)) + ":" + helpPrintMethod(minutes) + ":" +
                 helpPrintMethod(seconds) + " " + printAmPm(hours) );
    }

    /**
     * Вспомогательный метод для вывода времени, текущее значение меньше 10, то к значению добавляется символ 0
     * @param value текущее значение меньше
     * @return возвращаемое значение
     */
    private static String helpPrintMethod (int value){
        if (value > 9){
            return value + "";
        } else {
            return "0" + value;
        }
    }

    /**
     * Вспомогательный метод для вывода времени, в 12 часовом формате
     * @param hours часы
     * @return возвращает am или pm в зависмиоти от прошедших часов от полудня
     */
    private static String printAmPm(int hours){
        if (hours < 12){
            return "am";
        } else {
            return "pm";
        }
    }

    /**
     * Метод, который прибавляет переданное время к установленному в
     * TimeUnit (на вход передаются только часы, минуты и секунды).
     * @param addHours часы
     * @param addMinutes минуты
     * @param addSeconds секунды
     */
    public void addTime(int addHours, int addMinutes, int addSeconds){
        if((addSeconds + seconds) < 60){
            seconds += addSeconds;
        } else {
            seconds = addSeconds + seconds - 60;
            minutes++;
        }
        if((addMinutes + minutes) < 60){
            minutes += addMinutes;
        } else {
            minutes = minutes + addMinutes - 60;
            hours++;
        }
        if ((addHours + hours) < 24){
            hours += addHours;
        } else {
            hours = (hours + addHours) % 24;
        }
    }
}
