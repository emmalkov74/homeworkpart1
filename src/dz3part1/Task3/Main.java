package dz3part1.Task3;

import dz3part1.Task2.Student;

import java.util.Arrays;

public class Main {
    public static void main (String[] args) {
        Student[] students = new Student[]{
                new Student("Kevin", "Durant", new int[]{1,3,7,9,6,8,2}),
                new Student("Lorenzo", "Bol", new int[]{1,5,3,7,9}),
                new Student("Pol", "Jorge", new int[]{1,3,7,9,0,5,9})
        };
        StudentService.sortBySurname(students);

        for (Student s: students) {
            printStudent(s);
            System.out.println();
        }

        System.out.println("__________");

        System.out.println("Лучший студент: ");
        printStudent( StudentService.bestStudent(students));
    }


    private static void printStudent (Student student) {
        System.out.println("Name: " + student.getName());
        System.out.println("Surname: " + student.getSurname());
        System.out.println("Last 10 grades: " + Arrays.toString(student.getGrades()));
        System.out.printf("Arithmetic mean: %.2f \n" , student.arithmeticMean());

    }


}
