package pmdz3.Task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
Есть класс APrinter:
public class APrinter {
    public void print(int a) {
        System.out.println(a);
    }
}
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */
public class Main {

    public static void main (String[] args)  {

       int a = (int) (Math.random() * 1000);

       System.out.println("a = " + a);

        System.out.println("Вызываем с помощью рефлексии метод print() класса APrinter в качестве аргумента передаваем число a:");

        APrinter aPrinter = new APrinter();

        Class<?> cls = aPrinter.getClass();
        try {
            Method method = cls.getMethod("print", int.class);
            method.invoke(aPrinter, a);
        } catch (NoSuchMethodException e){
            System.out.println("Метод print не существует в классе APrinter");
        }catch (InvocationTargetException e) {
            System.out.println(e.getCause().getMessage());
        } catch (IllegalAccessException e) {
            System.out.println("Нет доступа к методу print");
        } catch (IllegalArgumentException e){
            System.out.println("В метод print передан неправильный аргумент");
        }

    }

}
