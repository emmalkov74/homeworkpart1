package pmdz3.AddTask1;
/*
Дана строка, состоящая из символов “(“ и “)”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Строка является правильной скобочной последовательностью, если:
●
Пустая строка является правильной скобочной последовательностью.
●
Пусть S — правильная скобочная последовательность, тогда (S) есть
правильная скобочная последовательность.
●
Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
есть правильная скобочная последовательность.

Входные данные             Выходные данные
(()()())                    true
)(                          false
(()                         false
((()))                      true
 */

import java.util.*;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку которую нужно проверить является ли она правильной скобочной последовательностью: ");
        String s = scanner.nextLine();
        boolean isValidBrackets =  checkIsValidBrackets(s);
        System.out.println(isValidBrackets);
    }

    /**
     * Метод, принимающий эту строку и выводящий результат, является ли она правильной скобочной последовательностью или нет,
     * по следующему алгоритму:
     * Eсли в проверяемой строке попадается открывающая скобка, то мы ее просто кладем в стэк, если попадается
     * закрывающая скобка то мы удаляем, последнюю открывающую скобку из стэка, причем если стэк оказался пуст возвращаем сразу false
     * В конце перебора всех символов в строке стэк обязательно должен быть пуст
     * @param s  проверяемая строка
     * @return логическое значение
     */
    private static boolean checkIsValidBrackets (String s) {
        Map<Character, Character> brackets = new HashMap<>();
        brackets.put(')','(');
        Deque<Character> stack = new LinkedList<>();
        for (char c: s.toCharArray()){
            if (brackets.containsValue(c)){
                stack.push(c);
            } else if (brackets.containsKey(c)) {
                if (stack.isEmpty()){
                    return false;
                } else {
                    stack.pop();
                }

            }
        }
        return stack.isEmpty();
    }
}
