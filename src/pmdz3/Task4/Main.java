package pmdz3.Task4;
/*
Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 */

import java.util.*;

public class Main {
    public static void main (String[] args) {
        NavigableSet<String> set = new TreeSet<>();
        Set<Class> interfaces =  getAllInterfacesOfClasses(set);
        interfaces = getAllInterfacesOfInterfaces(interfaces);
        for (Class c: interfaces) {
            System.out.println(c + " ");
        }
    }
    /**
     * метод, который с помощью рефлексии получает все интерфейсы класса переданного на вход объекта, включая интерфейсы
     * от классов-родителей  и возвращает их в виде списка
     * @param t переданный на вход объект
     * @param <T> обобщенный тип переданного на вход объекта
     * @return список всех интерфейсов класса переданного на вход объекта
     */
    private static <T> Set<Class> getAllInterfacesOfClasses (T t) {

        Set<Class> listInterfaces = new HashSet<>();
        Class thisClass = t.getClass();
        while (!thisClass.getSimpleName().equals("Object")){
            Class[] interfaces = thisClass.getInterfaces();
            for (Class c: interfaces) {
                listInterfaces.add(c);
            }
            thisClass = thisClass.getSuperclass();
        }
        return listInterfaces;
    }
    /**
     * Метод, который с помощью рефлексии получит все интерфейсы класса, включая интерфейсы от классов-родителей и интерфейсов-родителей
     * @param interfaces множество всех интерфейсы класса, включая интерфейсы от классов-родителей
     * @return множество содержащее все интерфейсы класса, включая интерфейсы от классов-родителей и интерфейсов-родителей
     */
    private static Set<Class> getAllInterfacesOfInterfaces (Set<Class> interfaces) {
        Set<Class> interfacesOfInterfaces = new HashSet<>();
        for (Class i: interfaces){
            interfacesOfInterfaces.addAll(findSuperInterfaces(i));
        }
        interfaces.addAll(interfacesOfInterfaces);
        return interfaces;
    }

    /**
     * Метод, который получает на вход класс интерфейса и возвращает коллекцию всех его интерфейсов и интерфейсов-родителей
     * @param i класс интерфейса
     * @return коллекция всех интерфейсов переданного на вход класса интерфейса и интерфейсов-родителей
     */
    private static Collection<Class> findSuperInterfaces (Class i) {
        Collection<Class> superInterfaces = new HashSet<>();
        Class thisClass = i.getClass();
        while (!thisClass.getSimpleName().equals("Object")){
            Class[] interfaces = thisClass.getInterfaces();
            for (Class c: interfaces) {
                superInterfaces.add(c);
            }
            thisClass = thisClass.getSuperclass();
        }

        return superInterfaces;
    }

}
