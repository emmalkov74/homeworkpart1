package pmdz3.AddTask2;
/*
Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Условия для правильной скобочной последовательности те, же что и в задаче 1,
но дополнительно:
●
Открывающие скобки должны быть закрыты однотипными
закрывающими скобками.
●
Каждой закрывающей скобке соответствует открывающая скобка того же
типа.

Входные данные          Выходные данные
{()[]()}                true
{)(}                    false
[}                      false
[{(){}}][()]{}          true
 */

import java.util.*;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку которую нужно проверить является ли она правильной скобочной последовательностью: ");
        String s = scanner.nextLine();
        boolean isValidBrackets =  checkIsValidBrackets(s);
        System.out.println(isValidBrackets);
    }

    /**
     * Метод, принимающий эту строку и выводящий результат, является ли она правильной скобочной последовательностью или нет,
     * по следующему алгоритму:
     * Eсли в проверяемой строке попадается открывающая скобка, то мы ее просто кладем в стэк, если попадается
     * закрывающая скобка, то мы проверяем, если стек пуст или текущая скобка не соответствует типу последней открывающей скобки,
     * то возвращаем сразу false
     * После проверки всех символов строки стэк должен быть обязательно пуст
     * @param s  проверяемая строка
     * @return логическое значение
     */
    private static boolean checkIsValidBrackets (String s) {
        Map<Character, Character> brackets = new HashMap<>();
        brackets.put(')','(');
        brackets.put('}', '{');
        brackets.put(']', '[');
        Deque<Character> stack = new LinkedList<>();
        for (char c: s.toCharArray()){
            if (brackets.containsValue(c)){
                stack.push(c);
            } else if (brackets.containsKey(c)) {
                if (stack.isEmpty() || stack.pop() != brackets.get(c)){
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
