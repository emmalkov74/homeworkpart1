package pmdz3.Task1;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Аннотация имеющая единственное поле isLike имеющее логический тип и по умолчанию имеющее значение false.
 * Для того чтобы аннотация была доступна во время выполнения программы ее разметили аннотацией @Retention с
 * указанием доступности в жизненном цикле кода RetentionPolicy.RUNTIME
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface IsLike {
   boolean isLike() default false;
}
