package pmdz3.Task2;

import pmdz3.Task1.IsLike;

/**
 * Пустой класс размеченный аннотацией @IsLike с полем isLike установленным в значение true
 */
@IsLike(
        isLike = true
)
public class MyClass {
}
