package pmdz3.Task2;
/*
Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на
экран.
 */

import pmdz3.Task1.IsLike;

import java.lang.annotation.Annotation;

public class Main {
    public static void main (String[] args) {
        MyClass myClass = new MyClass();
        Class clazz = myClass.getClass();

        try {
            boolean isLikeAnnotationValue = findIsLikeAnnotationValue(clazz);
            System.out.println(isLikeAnnotationValue);
        } catch (NullPointerException e){
            System.out.println("Данный класс не размечен аннотацией @IsLike");
        }

    }

    /**
     * Метод принимающий на вход экземпляр класса Class, который получает ссылку на объект аннотации IsLike и возвращает
     * значение его логического поля isLike
     * Метод может выкинуть исключение NullPointerException в том случае если принимаемый экземпляр класса не размечен
     * аннотацией isLike
     * @param clazz экземпляр класса Class
     * @return значение логического поля isLike аннотации IsLike
     */
    private static boolean findIsLikeAnnotationValue (Class clazz) {

            Annotation annotation = clazz.getAnnotation(IsLike.class);
            IsLike isLike = (IsLike) annotation;
            return isLike.isLike();

    }
}
