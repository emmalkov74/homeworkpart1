package dz1part3;
/*
На вход последовательно подается возрастающая последовательность из n
целых чисел, которая может начинаться с отрицательного числа.
Посчитать и вывести на экран, какое количество отрицательных чисел было
введено в начале последовательности. Помимо этого нужно прекратить
выполнение цикла при получении первого неотрицательного числа на вход.
Ограничения:
0 < n < 1000
-1000 < ai < 1000
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] str = scanner.nextLine().split(" ");
        int counter = 0;
        for (String s: str){
            if(Integer.parseInt(s) < 0){
                counter++;
            } else {
                break;
            }
        }
        System.out.println(counter);
    }

}
