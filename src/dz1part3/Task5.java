package dz1part3;

import java.util.Scanner;

/*
    Даны положительные натуральные числа m и n. Найти остаток от деления m на
    n, не выполняя операцию взятия остатка.
    Ограничения:
    0 < m, n < 10
 */
public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int temp = 0;

        if (n == 1){
            System.out.println(temp);
        } else {
            while (n * temp < m){
                temp++;
            }
            System.out.println(m - (temp - 1) * n);
        }
    }
}
