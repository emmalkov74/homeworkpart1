package pmdz6.AddTask2;

import java.util.Scanner;

/*
Напишите программы, чтобы узнать, является ли введенное число простым или нет.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число: ");
        int n = scanner.nextInt();
        String s = checkSimpleNumber(n) ? "Число " + n + " является простым"
                : "Число " + n + " не является простым";
        System.out.println(s);
    }

    /**
     * Метод возвращающий логическое значение является ли провееряемое число простым или нет
     * @param n проверяемое значение
     * @return логическое значение
     */
    private static boolean checkSimpleNumber(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0){
               return false;
            }
        }
        return true;
    }
}
