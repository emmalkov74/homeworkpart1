package pmdz6.AddTask1;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.util.Arrays.stream;

/*
Напишите программу для проверки, является ли введение число - числом Армстронга
Число Армстронга — натуральное число, которое в данной системе счисления равно сумме
своих цифр, возведенных в степень, равную количеству его цифр. Иногда, чтобы считать
число таковым, достаточно, чтобы степени, в которые возводятся цифры, были равны m.
Например, десятичное число
153— число Армстронга, потому что
1^3 + 5^3 + 3^3 = 153
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите натуральное число: ");
        int n = scanner.nextInt();

        String answer = isArmstrongNumber(n) ? "Число " + n + " является числом Армстронга"
                : "Число " + n + " не является числом Армстронга";

        System.out.println(answer);

    }

    /**
     * Метод возвращает логическое значение является ли подаваемое на вход число числом Армстронга
     * @param n входное значение
     * @return логическое значение
     */
    private static boolean isArmstrongNumber(int n) {
        int res = 0, temp = n;
        int length = ("" + n).length();

        while (n != 0) {
            int d = n % 10;
            res += Math.pow(d, length);
            n /= 10;
        }
        return res == temp;
    }
}
