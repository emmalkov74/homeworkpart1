package pmdz4.Task1;

import java.util.stream.Stream;

/*
Используя стримы:
1. Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран
 */
public class Main {
    public static void main (String[] args) {
        int sumEvenNumber =  Stream.iterate(1, n -> n + 1)
                .limit(100)
                .filter(o -> o % 2 == 0)
                .reduce((s1, s2) -> s1 + s2)
                .orElse(0);
        System.out.println(sumEvenNumber);

    }


}
