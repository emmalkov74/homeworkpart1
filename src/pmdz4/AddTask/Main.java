package pmdz4.AddTask;

import java.util.Scanner;


/*
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false

“tcat” “cat” -> true
“cat” “csat” -> true

 */
public class Main {
    public static void main (String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите первую стоку: ");
        String s1 = scanner.nextLine();
        System.out.println("Введите вторую стоку: ");
        String s2 = scanner.nextLine();

        System.out.println("\"" + s1 + "\"" + " \"" + s2 + "\" -> " + compareTwoStrings(s1, s2));
    }

    /**
     *  Метод на вход которого подается две строки и он возвращает логическое значение можно ли уравнять эти две строки,
     *  применив только одну из трех возможных операций:
     * 1. Добавить символ
     * 2. Удалить символ
     * 3. Заменить символ
     * @param s1  первая строка
     * @param s2 вторая строка
     * @return возвращаемое логическое значение
     */
    private static boolean compareTwoStrings (String s1, String s2) {

        char[] chars1 = s1.toCharArray();
        char[] chars2 = s2.toCharArray();
        char[] chars1Reverse = reverseStringToCharArray(s1);
        char[] chars2Reverse = reverseStringToCharArray(s2);

        if(Math.abs(chars1.length - chars2.length) > 1){
            return false;
        } else {
            return checkCharsArrays(chars1, chars2) || checkCharsArrays(chars1Reverse, chars2Reverse);
        }
    }

    /**
     * Вспомогательный метод получающий из строки массив символов предварительно разворачивая строку на оборот
     * @param str передаваемая на вход строка
     * @return массив символов получаемый из перевернутой строки
     */
    private static char[] reverseStringToCharArray(String str) {
        return new StringBuilder(str).reverse().toString().toCharArray();
    }

    /**
     * Вспомогательный метод принимающий на вход два массива символов, определяющий количество расхождений в них меньше двух или нет
     * @param chars1 первый массив символов
     * @param chars2 второй массив символов
     * @return логическое значение отражающее количество расхождений в массивах меньше двух или нет
     */
    private static boolean checkCharsArrays(char[] chars1, char[] chars2){
        int counter = 0;
        int minCharsLength = chars1.length <= chars2.length ? chars1.length : chars2.length;
        for (int i = 0; i < minCharsLength; i++) {
            if(chars1[i] != chars2[i]){
                counter++;
            }
        }
        return counter < 2;
    }
}
