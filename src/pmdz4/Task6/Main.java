package pmdz4.Task6;

import java.util.Set;
import java.util.TreeSet;

/*
Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>
 */

public class Main {
    public static void main (String[] args) {
        Set<Integer> set1 = Set.of(1,2,3,4,5);
        Set<Integer> set2 = Set.of(6,7,8,9);
        Set<Set<Integer>> set = Set.of(set1,set2);

        Set<Integer> result = new TreeSet<>();
                set.stream()
                .forEach(s -> s.forEach(integer -> result.add(integer)));
                result.forEach(i -> System.out.print(i + " "));
    }
}
