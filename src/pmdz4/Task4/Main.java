package pmdz4.Task4;
/*
На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */


import java.util.List;

public class Main {
    public static void main (String[] args) {
        List<Double> list = List.of(15.3, -266.54, 1852.3, 243.258, -658.3, 0.0, 3.0);

        list.stream()
                .sorted(((o1, o2) -> -o1.compareTo(o2)))
                .forEach(d -> System.out.print( d + " "));

    }


}
