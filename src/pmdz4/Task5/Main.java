package pmdz4.Task5;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/*
На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.

 */
public class Main {
    public static void main (String[] args) {
        List<String> list = List.of("abc", "def", "qqq");


        String result = list.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "));
        System.out.println(result);

    }
}
