package pmdz4.Task2;
/*
На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */


import java.util.List;

public class Main {
    public static void main (String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5);
        resultOfMultiplication(list);
    }

    /**
     * Метод на вход которого подается список целых чисел и он выводит результат перемножения этих чисел на экран
     * @param list список целых чисел
     */
    private static void resultOfMultiplication (List<Integer> list) {
        System.out.println( list.stream()
                .reduce((x,y) -> x * y)
                .get());
    }

}
