package pmdz4.Task3;
/*
На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3
 */

import java.util.List;

public class Main {
    public static void main (String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");
        System.out.println(findNumberOfNotEmptyStrings(list));
    }

    /**
     * Метод, который возвращает количество непустых строк в переданном на вход списке.
     * @param list переданный на вход список
     * @return количество непустых строк
     */
    private static int findNumberOfNotEmptyStrings (List<String> list) {
        return (int) list.stream()
                .filter(s -> s != "")
                .count();
    }
}
