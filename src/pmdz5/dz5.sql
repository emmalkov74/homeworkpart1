-- создаем таблицу цветы с ключом id и полями название цветка и его цена
create table flowers (
    id serial primary key ,
    flowerName varchar(50) not null unique,
    price money not null
);
-- заполняем таблицу цветы указанными в задании значениями

insert into flowers (flowername, price)
values
('Роза', 100),
('Лилия', 50),
('Ромашка', 25);

-- создаем таблицу покупателей с ключом id и полями имя покупателя и его телефон
create table customers(
     id serial primary key,
     customerName varchar(30) not null,
     telephon varchar(30) not null unique
);

-- заполняем таблицу покупатели первыми пришедшими на ум значениями
insert into customers (customername, telephon)
values
('Илья', '8-913-458-22-55'),
('Данила', '8-913-458-22-33'),
('Иван', '8-913-458-22-88'),
('Игорь', '8-913-458-22-99'),
('Виктор', '8-913-458-22-00'),
('Иван', '8-913-458-22-11'),
('Андрей', '8-913-458-33-55');

-- создаем таблицу заказы с ключом id и полями id покупателя, id цветка связывающими поля этой таблицы с полями в соответствующих таблицах
-- плюс добавлены поля количество цветов в заказе с установленными в задании ограничениями и полем дата заказа
create table orders(
     id serial primary key,
     customerid integer not null references customers(id) on delete set null,
     flowerid integer not null references flowers(id) on delete set null,
     numberofflowers integer check (numberofflowers > 0 and numberofflowers < 1000),
     orderdate timestamp not null
);

-- заполняем таблицу заказы первыми пришедшими на ум значениями
insert into orders (customerid, flowerid, numberofflowers, orderdate)
values
(5, 1, 20, now()),
(2, 3, 50, now() - interval '5m'),
(1, 2, 800, now() - interval '10d'),
(6, 3, 50, now() - interval '15d'),
(7, 1, 50, now() - interval '25d'),
(2, 3, 50, now() - interval '50d'),
(3, 1, 20, now() - interval '2d'),
(3, 2, 50, now() - interval '5d'),
(3, 3, 50, now() - interval '50d');

-- 1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
-- для примера выведем данные по заказу с идентификатором равным 3

select o.numberofflowers Количество_цветов,
       o.orderdate Дата_заказа,
       f.flowername Наименование_цветов,
       c.customername Имя_покупателя,
       c.telephon Телефон
from orders o
         join flowers f on f.id = o.flowerid
         join customers c on c.id = o.customerid
where o.id = 3;

--2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
-- получаем все данные по заказам клиента с идентификатором 3 за последний месяц

select o.numberofflowers Количество_цветов,
       o.orderdate Дата_заказа,
       f.flowername Наименование_цветов,
       c.customername Имя_покупателя,
       c.telephon Телефон
from orders o
         join flowers f on f.id = o.flowerid
         join customers c on c.id = o.customerid
where o.customerid = 3 and orderdate >= now() - interval '1 mons';

--3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество

select f.flowername Наименование_цветов,
       o.numberofflowers Количество_цветов
from orders o
         join flowers f on f.id = o.flowerid
         join customers c on c.id = o.customerid
where numberofflowers = (select max(numberofflowers) from orders);

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время

select sum(o.numberofflowers * f.price) Общая_выручка
from orders o
         join flowers f on f.id = o.flowerid;





