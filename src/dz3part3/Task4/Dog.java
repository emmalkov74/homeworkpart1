package dz3part3.Task4;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Класс собака
 */
public class Dog {

    // имя собаки
    private String dogName;

    //массив из оценок собаки
    private int[] scores;

    //Id хозяина
    private int participantId;


    /**
     * Конструктор создает участника конкурса собаку по ее имени и Id хозяина
     * @param dogName имя собаки
     * @param participantId Id хозяина
     */
    public Dog (String dogName, int participantId) {
        this.dogName = dogName;
        this.participantId = participantId;
    }

    /**
     * Сеттер устанавливающий оценки собаки
     * @param scores массив оценок
     */
    public void setScores (int[] scores) {
        this.scores = scores;
    }

    /**
     * Геттер возвращающий имя собаки
     * @return имя собаки
     */
    public String getDogName () {
        return dogName;
    }


    /**
     * Метод возвращающий среднее арифметическое оценок собаки
     * @return среднее арифметическое оценок собаки округленное до одного знака вниз
     */
    public double arithmeticMeanScore(){
        double sum = 0;
        for (int i: scores) {
           sum += i;
        }
        return sum / scores.length;
    }

}
