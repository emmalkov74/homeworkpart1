package dz3part3.Task4;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Класс конкурс собак хранит в себе всех участников и имеет метод для определения трех самых лучших
 */
public class Competition {

    // список участников (хозяев собак)
    private ArrayList<Participant> list;

    //Геттер возвращающий список участников (хозяев собак)
    public ArrayList<Participant> getList () {
        return list;
    }

    /**
     * конструктор создающий объект конкурса с пустым списком
     */
    public Competition () {
        this.list = new ArrayList<>();
    }


    /**
     *  метод для определения трех самых лучших участников
     * @return массив из трех самых лучших участников
     */
    public Participant[] threeBestDogs(){
        ArrayList<Participant> newList = (ArrayList<Participant>) list.clone();
        newList.sort(Participant::compareTo);
        Participant[] array = new Participant[3];
        int index = 0;
        for (int i = newList.size() - 1; i >= newList.size() - 3 ; i--) {
            array[index] = newList.get(i);
            index++;
        }
        return array;
    }



}
