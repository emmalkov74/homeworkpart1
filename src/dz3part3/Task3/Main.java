package dz3part3.Task3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.
Ограничения:
● 0 < N < 100
● 0 < M < 100
 */
public class Main {
    public static void main (String[] args) {
        //запрашиваем количество столбцов и строк в двумерном массиве
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество столбцов в двумерном массиве: ");
        int n = scanner.nextInt();
        System.out.println("Введите количество строк в двумерном массиве: ");
        int m = scanner.nextInt();

        //создаем список, который представляет из себя ArrayList каждым элементом которого будет являться одномерный массив int[],
        //а каждым элементом одномерного массива будет элемент значение которого будет суммой его индекса и индекса его массива в списке
        List<int[]> list = new ArrayList<>();

        //заполняем список
        for (int i = 0; i < m; i++) {
            int[] arr = new int[n];
            for (int j = 0; j < n; j++) {
                arr[j] = i + j;
            }
            list.add(i,arr);
        }
        //выводим матрицу на экран
        for (int[] arr: list) {
            for (int i = 0; i < arr.length; i++) {
                if(i != arr.length - 1){
                    System.out.print(arr[i] + " ");
                } else {
                    System.out.println(arr[i]);
                }
            }
        }

    }
}
