package dz3part3.Task1;

/**
 * Конечный класс Летучая мышь, она живородящая поэтому наследуется от класса Mammal и летает поэтому имплементируется
 * от интерфейса Flying
 */
public class Bat extends Mammal
        implements Flying{

    /**
     * Поле которое отражает скорость полета животного, в данном случае мышь летает медленно
     */
    private String howItFly = "медленно";


    /**
     * Метод который должны реализовать все летающие животные
     */
    @Override
    public void fly () {
        System.out.println("Летит: " + howItFly);
    }

    public String toString(){
        return "Летучая мышь";
    }
}
