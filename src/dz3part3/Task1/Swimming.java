package dz3part3.Task1;

/**
 * Этот интерфейс имплементируют все плавающие животные. Они должны реализовать в конечном классе метод swim
 */
public interface Swimming {
    /**
     * Метод который должны реализовать все плавающие животные
     */
    public void swim();
}
