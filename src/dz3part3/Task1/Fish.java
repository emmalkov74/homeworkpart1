package dz3part3.Task1;

/**
 * Подкласс животных рыбы. Все наследующие этот класс животные будут метать икру
 */
public class Fish extends Animal {
    /**
     * метод выводит на экран как животное рожает: мечет икру
     */
    @Override
    public void wayOfBirth () {
        System.out.println("Как животное рожает: мечет икру");
    }
}
