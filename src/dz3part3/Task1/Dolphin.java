package dz3part3.Task1;

/**
 *Конечный класс Дельфин, он живородящий поэтому наследуется от класса Mammal и плавает поэтому имплементируется
 *  * от интерфейса Swimming
 */
public class Dolphin extends Mammal
        implements Swimming {

    /**
     *Поле которое отражает скорость плавания животного, в данном случае дельфин плавает быстро
     */
    private String howItSwim = "быстро";

    /**
     * Метод который должны реализовать все плавающие животные
     */
    @Override
    public void swim () {
        System.out.println("Плавает: " + howItSwim);
    }

    public String toString(){
        return "Дельфин";
    }
}
