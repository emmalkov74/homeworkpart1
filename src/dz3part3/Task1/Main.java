package dz3part3.Task1;
/*
Рассматриваются следующие животные:
● летучая мышь (Bat)
● дельфин (Dolphin)
● золотая рыбка (GoldFish)
● орел (Eagle)
Все животные одинаково едят и спят (предположим), и никто из животных не
должен иметь возможности делать это иначе.
Еще животные умеют по-разному рождаться (wayOfBirth):
● Млекопитающие (Mammal) живородящие.
● Рыбы (Fish) мечут икру.
● Птицы (Bird) откладывают яйца.
Помимо этого бывают некоторые особенности, касающиеся передвижения.
Бывают летающие животные (Flying) и плавающие (Swimming). Однако орел
летает быстро, а летучая мышь медленно. Дельфин плавает быстро, а золотая
рыбка медленно.
Согласно этим утверждениям нужно создать иерархию, состоящую из классов,
абстрактных классов и/или интерфейсов. Каждое действие или утверждение
подразумевает под собой вызов void метода, в котором реализован вывод на
экран описания текущего действия.

 */

public class Main {
    public static void main (String[] args) {
        Animal animal1 = new Bat();
        doAll(animal1);
        System.out.println("______________________");
        Animal animal2 = new Dolphin();
        doAll(animal2);
        System.out.println("______________________");
        Animal animal3 = new GoldFish();
        doAll(animal3);
        System.out.println("______________________");
        Animal animal4 = new Eagle();
        doAll(animal4);
    }

    private static void doAll (Animal animal) {
        System.out.println(animal.toString());
        animal.eat();
        animal.sleep();
        animal.wayOfBirth();
        if(animal instanceof Flying){
            ((Flying) animal).fly();
        }
        if (animal instanceof Swimming){
            ((Swimming) animal).swim();
        }
    }

}


