package dz3part3.Task1;

/**
 * Конечный класс Орел, он является птицей, поэтому наследуется от класса Bird и летает поэтому имплементируется
 *  от интерфейса Flying
 */
public class Eagle extends Bird
            implements Flying{

    /**
     * Поле которое отражает скорость полета животного, в данном случае орел летает быстро
     */
    private String howItFly = "быстро";



    /**
     * Метод который должны реализовать все летающие животные
     */
    @Override
    public void fly () {
        System.out.println("Летит: " + howItFly);
    }

    public String toString(){
        return "Орел";
    }
}
