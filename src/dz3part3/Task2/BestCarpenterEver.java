package dz3part3.Task2;

/**
 * Класс, который создает экземпляр цеха по ремонту BestCarpenterEver Мебели. Он умеет чинить лишь некоторую Мебель.
 */
public class BestCarpenterEver {

    /**
     * Метод, который в зависимости от подкласса Мебели поступающей в качестве параметра, выводит сообщение:
     * может он ее починить или нет
     * @param furniture Мебель
     */
    public void repair(Furniture furniture){
        if(furniture instanceof Stool){
            System.out.println("Цех по ремонту BestCarpenterEver умеет чинить Табуретки");
        }
        if (furniture instanceof Table){
            System.out.println("Цех по ремонту BestCarpenterEver умеет чинить только Табуретки, а Столы нет");
        }
    }
}
